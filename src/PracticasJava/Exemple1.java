package PracticasJava;

import java.util.Scanner;

public class Exemple1 {
	static Scanner sc;

public static void main(String[] args) {
		
		sc = new Scanner(System.in);

		int num1=0;
		int num2=0;
		
		int opcio = -1;
		do {						
			opcio = menu();
			switch (opcio) {
			case 0:
				break;
			case 1:
				num1 = obtenirNumero();
				break;
			case 2:
				num2 = obtenirNumero();
				break;
			case 3:
				num1 = obtenirNumero();
				num2 = obtenirNumero();
				break;
			case 4: //visualitzar els n�meros
				visualitzarNumeros (num1, num2);
				break;
			case 5:
				System.out.println("El m�s gran �s: " + mesGran(num1, num2));
				break;
			case 6:
				System.out.println("El promig �s: " + promig(num1, num2));
				break;
			default:
				break;
			}
			System.out.println("opci� de l'usuari �s " + opcio);
		} while (opcio != 0);
		// missatge de comiat
	}

	private static int menu() {
		int opc;
		do {
			System.out.println("\n\nPrograma per veure funcions");
			System.out.println("***************************\n");
			System.out.println("1.- Obtenir primer n�mero");
			System.out.println("2.- Obtenir segon n�mero");
			System.out.println("3.- Obtenir els dos n�meros");
			System.out.println("4.- Visualitzar els n�meros");
			System.out.println("5.- Saber quin �s el m�s gran");
			System.out.println("6.- Promedio");
			System.out.println("0.- Sortir");
			System.out.print("Digues la teva opci�: ");
			opc = sc.nextInt();
		} while (opc < 0 || opc > 6);

		return (opc);
	}

	private static int obtenirNumero() {
		System.out.println("Digues un n�mero(1..10):");
		return(sc.nextInt());
	}

	private static void visualitzarNumeros(int a, int b) {
		System.out.println("N�mero1: " + a + " N�mero2: " + b);
	}
	
	private static int mesGran(int a, int b) {
		if(a >b) 
			return a;
		else
			return b;
	}
	
	private static double promig(int a,int b) {
		return ((double)(a+b))/2.0;
	}
}
